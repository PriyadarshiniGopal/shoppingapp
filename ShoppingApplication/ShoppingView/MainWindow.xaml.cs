﻿using System.Windows;
using ShoppingViewModel;

namespace ShoppingView
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Bind product details from viewmodel to main window
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            // Set DataContext as View Model
            ProductViewModel viewmodel = new ProductViewModel();
            this.DataContext =viewmodel;
        }
    }
}
