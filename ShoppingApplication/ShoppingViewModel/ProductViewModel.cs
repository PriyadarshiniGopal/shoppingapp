﻿using System.ComponentModel;
using System.Collections.ObjectModel;
using ShoppingModel;
using CommandInterface;
using System.Windows;
using Microsoft.VisualBasic;
using System;

namespace ShoppingViewModel
{
    /// <summary>
    /// ViewModel  to Get product details from Model
    /// </summary>
    public class ProductViewModel:INotifyPropertyChanged
    {
        #region Property Changed Handler
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string PropertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }
        #endregion

        #region ProductDetail Property
        ObservableCollection<Product> _productDetail;
        public ObservableCollection<Product> ProductDetail
        {
            get
            {
                return _productDetail;
            }
            set
            {
                _productDetail = value;
                OnPropertyChanged("ProductDetail");
            }
        }
        #endregion

        #region Constructor 
        ProductService Service;
        /// <summary>
        /// Instantiate Product Service
        /// </summary>
        public ProductViewModel()
        {
            Service = new ProductService();
            LoadData();
        }
        #endregion

        /// <summary>
        /// Store all product Details
        /// </summary>
        public void LoadData()
        {
            ProductDetail=Service.GetAll();
        }

        #region Property CartItems
        ObservableCollection<Product> _cartItems=new ObservableCollection<Product>();
        public ObservableCollection<Product> CartItems
        {
            get
            {
                return _cartItems;
            }
            set
            {
                _cartItems = value;
                OnPropertyChanged("CartItems");
            }
        }
        #endregion

        #region Total Amount
        int _totalAmount;
        public int TotalAmount
        {
            get
            {
                return _totalAmount;
            }
            set
            {
                _totalAmount = value;
                OnPropertyChanged("TotalAmount");
            }
        }
        public void CalculateTotalAmount()
        {
            TotalAmount = 0;
            foreach (Product item in CartItems)
                TotalAmount += item.TotalPrice;
        }
        #endregion

        #region Add to Cart Command
        private RelayCommand addToCart;
        public RelayCommand AddToCart
        {
            get
            {
                if(addToCart==null)
                {
                    addToCart = new RelayCommand(param => this.AddItemsToCart(param));
                }
                return addToCart;
            }
        }
        /// <summary>
        /// Handler to add items to cart
        /// </summary>
        /// <param name="cartItem">Selected product from List</param>
        public void AddItemsToCart(object cartItem)
        {
            Product item= (Product)cartItem;
            try
            {
                int ProductCount = Int32.Parse(Interaction.InputBox("Enter No of Items to be added to cart", "Add Items", ""));
                if (ProductCount <= 0)
                {
                    MessageBox.Show("Invalid input for No of items to be added to cart !");
                    return;
                }
                if (item.InStock < ProductCount)
                {
                    MessageBox.Show("You can Only add " + item.InStock + " items at the maximum");
                    return;
                }
                item.NumAddedToCart += ProductCount;
                item.InStock-=ProductCount;
                if (CartItems.Contains(item) == false)
                    CartItems.Add(item);
                CalculateTotalAmount();
                if (item.InStock == 0)
                    ProductDetail.Remove(item);
                MessageBox.Show("Product Added to Cart");
            }
            catch
            {
                MessageBox.Show("Invalid input for No of items to be added to cart");
            }
        }
        #endregion

        #region Remove from Cart
        private RelayCommand removeFromCart;
        public RelayCommand RemoveFromCart
        {
            get
            {
                if (removeFromCart == null)
                {
                    removeFromCart = new RelayCommand(param => this.RemoveItemsFromCart(param));
                }
                return removeFromCart;
            }
        }
        /// <summary>
        /// Handler to remove items from cart
        /// </summary>
        /// <param name="cartItem">Selected product from List</param>
        public void RemoveItemsFromCart(object cartItem)
        {
            Product item = (Product)cartItem;
            try
            {
                // get Product count to remove items 
                int ProductCount = Int32.Parse(Interaction.InputBox("Enter No of Items to be Removed", "Remove Items", ""));
                if (ProductCount <= 0)
                {
                    MessageBox.Show("Invalid input for No of items to be removed from cart !");
                    return;
                }
                // Input is less than quantity added to cart
                if (item.NumAddedToCart < ProductCount)
                {
                    MessageBox.Show("You can Only remove " + item.NumAddedToCart + " items at the maximum");
                    return;
                }

                item.NumAddedToCart-=ProductCount;
                item.InStock++;
                if (item.NumAddedToCart == 0)
                    CartItems.Remove(item);
                if (item.InStock == 1)
                    ProductDetail.Add(item);
                // calculate Total Amount
                CalculateTotalAmount();
                MessageBox.Show("Product Removed from Cart");
            }
            catch
            {
                MessageBox.Show("Invalid input for No of items to be removed from cart !");
            }
        }
        #endregion

        #region Orders property
        ObservableCollection<Product> orders=new ObservableCollection<Product>();
        public ObservableCollection<Product> Orders
        {
            get
            {
                return orders;
            }
            set
            {
                orders = value;
                OnPropertyChanged("Orders");
            }
        }
        #endregion

        #region Place order command
        RelayCommand placeorder;
        public RelayCommand PlaceOrder
        {
            get
            {
                if (placeorder == null)
                {
                    placeorder = new RelayCommand(param => this.Payment(param));
                }
                return placeorder;
            }
        }
        public void Payment(object cartProducts)
        {
            foreach (Product item in (ObservableCollection<Product>)cartProducts)
            {
                Orders.Add(item);
            }
            CartItems.Clear();
            MessageBox.Show("Order Placed");
        }
        #endregion

        #region SelectedProduct Property
        Product _selectedProduct;
        public Product SelectedProduct
        {
            get
            {
                return _selectedProduct;
            }
            set
            {
                _selectedProduct = value;
                OnPropertyChanged("SelectedProduct");
            }
        }
        #endregion

        #region Popup open Property
        bool _isOpen;
        public bool IsOpen
        {
            get
            {
                return _isOpen;
            }
            set
            {
                _isOpen = value;
                OnPropertyChanged("IsOpen");
            }
        }
        #endregion

        #region Product Select Command
        RelayCommand select;
        public RelayCommand Select
        {
            get
            {
                if (select == null)
                    select = new RelayCommand(param => this.SetSelectedProduct(param));
                return select;
            }
        }
        /// <summary>
        /// To set selected product details
        /// </summary>
        /// <param name="item">Sellected item</param>
        public void SetSelectedProduct(object item)
        {
            SelectedProduct = (Product)item;
            IsOpen = true;
        }
        #endregion

        #region Product Unselect Command
        RelayCommand unSelect;
        public RelayCommand UnSelect
        {
            get
            {
                if (unSelect == null)
                    unSelect = new RelayCommand(param => this.UnSetSelectedProduct(param));
                return unSelect;
            }
        }
        /// <summary>
        /// Unselect the product
        /// </summary>
        /// <param name="item">selected product</param>
        public void UnSetSelectedProduct(object item)
        {
            IsOpen = false;
        }
        #endregion

        #region Favourite Products
        ObservableCollection<Product> _favItems = new ObservableCollection<Product>();
        public ObservableCollection<Product> FavItems
        {
            get
            {
                return _favItems;
            }
            set
            {
                _favItems = value;
                OnPropertyChanged("FavItems");
            }
        }

        #endregion

        #region Add to Favourites Command
        private RelayCommand favourite;
        public RelayCommand Favourite
        {
            get
            {
                if (favourite == null)
                {
                    favourite = new RelayCommand(param => this.AddOrRemoveFav(param));
                }
                return favourite;
            }
        }
        /// <summary>
        /// Handler to add items to cart
        /// </summary>
        /// <param name="cartItem">Selected product from List</param>
        public void AddOrRemoveFav(object favItem)
        {
            Product item = (Product)favItem;
            if (FavItems.Contains(item) == false)
            {
                FavItems.Add(item);
                item.IsAddedToFav = true;
            }
            else
            {
                FavItems.Remove(item);
                item.IsAddedToFav = false;
            }
        }
        #endregion

    }
}
