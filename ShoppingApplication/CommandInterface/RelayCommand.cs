﻿using System;
using System.Windows.Input;

namespace CommandInterface
{
    /// <summary>
    /// Relay Command
    /// </summary>
    public class RelayCommand : ICommand
    {
        #region ICommand Interface
        public event EventHandler CanExecuteChanged;

        Action<object> DoWork;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            DoWork(parameter);
        }
        #endregion

        public RelayCommand(Action<object> work)
        {
            DoWork = work;
        }
    }
}
