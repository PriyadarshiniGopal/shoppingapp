﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.ComponentModel;

namespace ShoppingModel
{
    public class Product : INotifyPropertyChanged
    {
        #region Properties for Products
        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        int id;
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
                OnPropertyChanged("Id");
            }
        }
        string desc;
        public string Description
        {
            get
            {
                return desc;
            }
            set
            {
                desc = value;
                OnPropertyChanged("Description");
            }
        }
        string brand;
        public string Brand
        {
            get
            {
                return brand;
            }
            set
            {
                brand = value;
                OnPropertyChanged("Brand");
            }
        }
        string color;
        public string Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
                OnPropertyChanged("Color");
            }
        }
        string imgsource;
        public string ImgSource
        {
            get
            {
                return imgsource;
            }
            set
            {
                imgsource = value;
                OnPropertyChanged("ImgSource");
            }
        }
        int price;
        public int Price
        {
            get
            {
                return price;
            }
            set
            {
                price = value;
                OnPropertyChanged("Price");
            }
        }
        int instock;
        public int InStock
        {
            get
            {
                return instock;
            }
            set
            {
                 instock = value;
                OnPropertyChanged("InStock");
            }
        }
        int rating;
        public int Rating
        {
            get
            {
                return rating;
            }
            set
            {
                rating = value;
                OnPropertyChanged("Rating");
            }
        }
        int numaddedtocart;
        public int NumAddedToCart
        {
            get
            {
                return numaddedtocart;
            }
            set
            {
                numaddedtocart = value;
                this.TotalPrice = this.NumAddedToCart * this.Price;
                OnPropertyChanged("NumAddedToCart");
            }
        }
        bool isaddedtofav;
        public bool IsAddedToFav
        {
            get
            {
                return isaddedtofav;
            }
            set
            {
                isaddedtofav = value;
                OnPropertyChanged("IsAddedToFav");
            }
        }
        int totalPrice;
        public int TotalPrice
        {
            get
            {
                return totalPrice;
            }
            set
            {
                totalPrice = value;
                OnPropertyChanged("TotalPrice");
            }
        }

        #endregion

        #region Property Changed handler
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string PropertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }
        #endregion
    }
}
