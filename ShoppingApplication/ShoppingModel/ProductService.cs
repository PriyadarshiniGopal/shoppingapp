﻿using System.Collections.ObjectModel;
using System.IO;
using Newtonsoft.Json.Linq;

namespace ShoppingModel
{
    /// <summary>
    /// To provide all product details and manipulations
    /// </summary>
    public class ProductService
    {

        ObservableCollection<Product> ProductList = new ObservableCollection<Product>();
        /// <summary>
        /// To Read product details from Json and store it in Product object
        /// </summary>
        public ProductService()
        {
            /// Read Json file and store it as a array
            JArray jsonProducts = JArray.Parse(File.ReadAllText("../../../ShoppingModel/ProductDetails.json"));
            /// Convert json Array objects to Product List object
            foreach (JObject products in jsonProducts)
            {
                Product productObject = products.ToObject<Product>();
                ProductList.Add(productObject);
            }
        }

        /// <summary>
        /// Return all products
        /// </summary>
        /// <returns>Collection contains Product Detail</returns>
        public ObservableCollection<Product> GetAll()
        {
            return ProductList;
        }
    }
}
